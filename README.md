![vanilla](https://img.shields.io/badge/DESIGNED%20FOR-VANILLA+%20SERVERS-green?style=for-the-badge)

# Retro's Arcane Forge
**Supported Versions:**

![1.14](https://img.shields.io/badge/1.14%20to%201.15-SUPPORTED-green?style=for-the-badge)

Retro's Forge adds a bit of flair in a vanilla feel to the blast furnace. Drop a nether star into a blast furnace and stand back! The furnace will be converted to an arcane furnace, which aside from looking totally bad a** increases the players yeilds from the furnace. Smelted ores can give up to a 5x multiplier! (confurable of course). They can only be picked up with a silk touch pickaxe, doing otherwise might have unforseen consequences.

![gif](https://i.imgur.com/szzFjYc.mp4)


## Build
To build clone the repo and run `mvn`. The compile plugin will be in the `target` directory.

## Vanilla Focused

The plugin is built to give a vanilla+ feel without too many in-your-face features.

Sounds, particles, and multipliers and more can be configured to your liking. Everything is craftable without the use of custom GUIs or commands and destruction/relocation was built to feel like it was in the game to begin with.

## In-Game Usage
#### Building the forge
Simply place a nether star into the blast furnace to craft the arcane forge.

#### Destruction
To move an arcane forge you will need to use a silk-touch pickaxe, otherwise the forge will explode catostrophically.

#### Ore Multiplication
Ore multiplication by default can give you 0-5x the output from smelting ores, this can be configured in `config.yml`

## Permissions
None needed!

## Configuration

### Max Yield
Change this to the maximum amount of ores you'd like to be able to obtain from smelting.
```yml
max-yield: 5
```

### Require Silk-Touch
Disabling this will allow you to deconstruct the forge using any pickaxe.
```yml
require-silk-touch: true
```

### Language, sounds & particles
All language, sounds and particles can also be configured.
```yml
language:
  depleted-nether-star: "Arcane Ash"
  depleted-nether-star-lore: "It smells faintly of a three-headed beast."
sounds:
  nether-star-depleted: ENTITY_WITHER_DEATH
  smelt: ENTITY_VEX_AMBIENT
  detonate: ENTITY_WITHER_BREAK_BLOCK
particles:
  spiral: ENCHANTMENT_TABLE
  passive: ENCHANTMENT_TABLE
```
