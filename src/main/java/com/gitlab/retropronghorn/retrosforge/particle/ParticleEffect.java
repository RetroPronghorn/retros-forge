package com.gitlab.retropronghorn.retrosforge.particle;

import java.util.Random;

import com.gitlab.retropronghorn.retrosforge.RetrosForge;
import com.gitlab.retropronghorn.retrosutils.particle.UParticle;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;

public class ParticleEffect {
    public static void createHelix(Location location) {
        Particle particle = (Particle) UParticle.particleOrFallback(
            RetrosForge.getPlugin().getConfig().getString("particles.spiral")
        );
        int radius = 2;
        for(double i = 0; i <= 10; i+=0.05) {
            double y = i;
            double x = (radius-y) * Math.cos(y);
            double z = (radius-y) * Math.sin(y);
            Bukkit.getScheduler().runTaskLater(RetrosForge.getPlugin(), new Runnable() {
                @Override
                public void run() {
                    location.getWorld().spawnParticle(
                        particle,
                        (float) (location.getX() + x),
                        (float) (location.getY() + y),
                        (float) (location.getZ() + z),
                        1
                    );
                }
            }, ((Double) ((10 - i)*2)).longValue());
        }
    }

    public static void createCloud(Location location, Random random) {
        Particle particle = (Particle) UParticle.particleOrFallback(
            RetrosForge.getPlugin().getConfig().getString("particles.passive")
        );

        for (int i = 0; i <= 10; i++) {
            double offsetX = random.nextDouble();
            double offsetY = random.nextDouble();
            double offsetZ = random.nextDouble();
            location.getWorld().spawnParticle(
                particle,
                (float) (location.getX() + offsetX),
                (float) (location.getY() + offsetY),
                (float) (location.getZ() + offsetZ),
                1
            );
        }
    }
}