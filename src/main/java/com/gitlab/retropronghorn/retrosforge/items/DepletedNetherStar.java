package com.gitlab.retropronghorn.retrosforge.items;

import java.util.Arrays;
import java.util.List;

import com.gitlab.retropronghorn.retrosforge.RetrosForge;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class DepletedNetherStar {
    /**
     * Builds a depleted netherstar item
     * @return returns itemstack representing a depleted nether star
     */
    public static ItemStack get() {
        ItemStack netherCoal = new ItemStack(Material.COAL);
        ItemMeta itemMeta = netherCoal.getItemMeta();
        // Set display name and lore
        itemMeta.setDisplayName(
            RetrosForge.getPlugin().getConfig().getString("language.depleted-nether-star")
        );
        List<String> lore = Arrays.asList(
            RetrosForge.getPlugin().getConfig().getString("language.depleted-nether-star-lore")
        );
        itemMeta.setLore(lore);
        netherCoal.setItemMeta(itemMeta);
        return netherCoal;
    }
}