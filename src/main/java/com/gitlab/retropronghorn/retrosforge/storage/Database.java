package com.gitlab.retropronghorn.retrosforge.storage;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import com.gitlab.retropronghorn.retrosforge.RetrosForge;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Database {
    File storageFile;
    protected final FileConfiguration storage;

    /**
     * Construct a Database
     * @param RetrosForge instace to main plugin
     **/
    public Database(RetrosForge inststance) {
        storageFile = new File(inststance.getDataFolder(), "data.yml");
        if (!storageFile.exists()) {
            storageFile.getParentFile().mkdirs();
            inststance.saveResource("data.yml", false);
        }
        storage = new YamlConfiguration();
        try {
            storage.load(storageFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set a new key value store and save
     *
     * @param node path to store at
     * @param value value to store
     */
    public void set(String node, Object value) {
        storage.set(node, value);
    }

    /**
     * Set a node to deleted
     *
     * @param node path to delete
     */
    public void remove(String node) {
        storage.set(node, null);
    }

    /**
     * Get a node value
     *
     * @param location node to get from
     */
    public Object get(String node) {
        return storage.get(node);
    }


    /**
     * Get a node value
     *
     * @param location node to get from
     */
    public List<String> getList(String node) {
        return storage.getStringList(node);
    }

    public Set<String> keys() {
        return storage.getKeys(false);
    }

    /**
     * Save the data storage file
     */
    public void save() {
        try {
            storage.save(storageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}