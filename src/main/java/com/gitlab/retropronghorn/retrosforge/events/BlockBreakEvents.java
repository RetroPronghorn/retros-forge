package com.gitlab.retropronghorn.retrosforge.events;

import com.gitlab.retropronghorn.retrosforge.RetrosForge;
import com.gitlab.retropronghorn.retrosforge.controllers.PickupController;
import com.gitlab.retropronghorn.retrosutils.version.UServer;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class BlockBreakEvents implements Listener {
    private RetrosForge instance;

    /**
     * Construct a new block break events listener
     * @param instance reference to the plugin instance
     */
    public BlockBreakEvents(RetrosForge instance) {
        this.instance = instance;
    }

    private static Boolean isSilkPickaxe(ItemStack item) {
        if (item.getType() == Material.WOODEN_PICKAXE ||
            item.getType() == Material.STONE_PICKAXE ||
            item.getType() == Material.IRON_PICKAXE ||
            item.getType() == Material.GOLDEN_PICKAXE ||
            item.getType() == Material.DIAMOND_PICKAXE) {
                return item.getEnchantments().containsKey(Enchantment.SILK_TOUCH);
            } else if (UServer.isServerVersion(UServer.V1_16) &&
                item.getType() == Material.NETHERITE_PICKAXE) {
                return item.getEnchantments().containsKey(Enchantment.SILK_TOUCH);
            }
        return false;
    }

    @EventHandler(priority = EventPriority.LOW)
    public void blockBreakEvent(BlockBreakEvent event) {
        if (event.getBlock().getType() == Material.BLAST_FURNACE && !event.isCancelled()) {
            ItemStack heldItem = event.getPlayer().getInventory().getItemInMainHand();
            Location location = event.getBlock().getLocation();
            if (instance.getConfig().getBoolean("require-silk-touch")) {
                if (instance.getFuranceLocations().contains(location)) {
                    if (isSilkPickaxe(heldItem)) {
                        PickupController.pickup(location);
                        instance.removeFurnace(location);
                    } else {
                        event.setCancelled(true);
                        PickupController.invalidPickup(location, event.getBlock());
                        instance.removeFurnace(location);
                    }
                }
            } else if (instance.getFuranceLocations().contains(location)) {
                PickupController.pickup(location);
                instance.removeFurnace(location);
            }
        }
    }
}