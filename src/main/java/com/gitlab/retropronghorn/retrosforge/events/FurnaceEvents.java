package com.gitlab.retropronghorn.retrosforge.events;

import com.gitlab.retropronghorn.retrosforge.RetrosForge;
import com.gitlab.retropronghorn.retrosforge.controllers.FurnaceController;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Furnace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.FurnaceSmeltEvent;
import org.bukkit.inventory.ItemStack;

public class FurnaceEvents implements Listener {
    private RetrosForge instance;

    /**
     * Constructs a new furnace events listener
     * @param instance reference to plugin instance
     */
    public FurnaceEvents(RetrosForge instance) {
        this.instance = instance;
    }

    @EventHandler
    public void furnaceSmeltEvent(FurnaceSmeltEvent event) {
        Location location = event.getBlock().getLocation();
        Furnace furnace = (Furnace) event.getBlock().getState();
        ItemStack smelting = event.getResult();
        if (instance.getFuranceLocations().contains(location)) {
            Bukkit.getScheduler().runTaskLater(RetrosForge.getPlugin(), new Runnable() {
                @Override
                public void run() {
                    FurnaceController.multiplyYield(furnace, smelting);
                }
            }, 1L);
        }
    }
}