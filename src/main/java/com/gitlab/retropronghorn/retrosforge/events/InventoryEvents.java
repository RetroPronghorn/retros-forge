package com.gitlab.retropronghorn.retrosforge.events;

import com.gitlab.retropronghorn.retrosforge.RetrosForge;
import com.gitlab.retropronghorn.retrosforge.controllers.InventoryController;

import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;

public class InventoryEvents implements Listener {
    private RetrosForge instance;
    private InventoryController controller;

    /**
     * Construct a new inventory events listener
     * @param instance reference to the plugin instance
     */
    public InventoryEvents(RetrosForge instance) {
        this.instance = instance;
        controller = new InventoryController();
    }

    @EventHandler
    public void inventoryClickEvent(InventoryClickEvent event) {
        if (event.isLeftClick() || event.isRightClick()) {
            if (event.getClickedInventory() != null &&
                event.getClickedInventory().getType() == InventoryType.BLAST_FURNACE) {
                HumanEntity actor = event.getWhoClicked();
                if (actor.getItemOnCursor().getType() == Material.NETHER_STAR) {
                    controller.handleFurnaceFueled(event, instance);
                }
            }
        }
    }
}
