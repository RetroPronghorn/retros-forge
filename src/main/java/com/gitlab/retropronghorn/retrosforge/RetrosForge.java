package com.gitlab.retropronghorn.retrosforge;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import com.gitlab.retropronghorn.retrosforge.events.BlockBreakEvents;
import com.gitlab.retropronghorn.retrosforge.events.FurnaceEvents;
import com.gitlab.retropronghorn.retrosforge.events.InventoryEvents;
import com.gitlab.retropronghorn.retrosforge.particle.ParticleEffect;
import com.gitlab.retropronghorn.retrosforge.storage.Database;
import com.gitlab.retropronghorn.retrosutils.location.ULocation;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;

public class RetrosForge extends JavaPlugin {
    public static RetrosForge retrosForge;
    private List<Location> furnaceLocations = new ArrayList<>();
    private static Random random = new Random();
    private int particleTask;
    private Database db;

    private void particleLoop() {
        furnaceLocations.forEach(location -> {
            ParticleEffect.createCloud(location, random);
        });
    }

    /**
     * @return returns reference to the plugin instance
     */
    public static RetrosForge getPlugin() {
        return retrosForge;
    }

    /**
     * Add a furnace to the list of arcane furnaces
     * @param loc location of the furance to add
     */
    public void addFurnace(Location loc) {
        furnaceLocations.add(loc);
    }

    /**
     * Remove a furnace from the list of arcane furances
     * @param loc location of the furnace to remove
     */
    public void removeFurnace(Location loc) {
        furnaceLocations.remove(loc);
    }

    /**
     * Get a list of the furnaces tracked
     * @return returns a list of the tracked furnaces
     */
    public List<Location> getFuranceLocations() {
        return furnaceLocations;
    }

    @Override
    public void onEnable() {
        retrosForge = this;
        saveDefaultConfig();
        // Register Events
        getServer().getPluginManager().registerEvents(new InventoryEvents(this), retrosForge);
        getServer().getPluginManager().registerEvents(new FurnaceEvents(this), retrosForge);
        getServer().getPluginManager().registerEvents(new BlockBreakEvents(this), retrosForge);
        // Start particle loop
        particleTask = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            public void run() {
                particleLoop();
            }
        }, 0, 20L);
        // Create DB
        db = new Database(this);
        List<String> stringLocations = db.getList("forges");
        stringLocations.forEach(l -> {
            addFurnace(ULocation.fromString(l));
        });
    }

    @Override
    public void onDisable() {
        // Cleanup particle loop
        Bukkit.getServer().getScheduler().cancelTask(particleTask);
        // Store generators
        List<String> stringLocations = furnaceLocations.stream()
            .map(l -> ULocation.toLocationString(l))
            .collect(Collectors.toList());
        db.set("forges", stringLocations);
        db.save();
    }
}