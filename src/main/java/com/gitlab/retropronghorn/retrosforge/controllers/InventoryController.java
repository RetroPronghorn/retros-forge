package com.gitlab.retropronghorn.retrosforge.controllers;

import com.gitlab.retropronghorn.retrosforge.RetrosForge;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Furnace;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.FurnaceInventory;

public class InventoryController {
    /**
     * Handles when a furnace is fueld to check for a nether star
     * @param event Click event we're acting con
     * @param instance Reference to the plugin instnace
     */
    public void handleFurnaceFueled(InventoryClickEvent event, RetrosForge instance) {
        FurnaceInventory furnaceInventory = (FurnaceInventory) event.getClickedInventory();
        Furnace furnace = (Furnace) event.getClickedInventory().getHolder();
        Bukkit.getScheduler().runTaskLater(RetrosForge.getPlugin(), new Runnable() {
            @Override
            public void run() {
                if (furnaceInventory.contains(Material.NETHER_STAR) &&
                    !instance.getFuranceLocations().contains(furnace.getLocation())) {
                    FurnaceController.depleteNetherStar(furnace);
                    instance.addFurnace(furnace.getLocation());
                }
            }
        }, 5L);
    }
}