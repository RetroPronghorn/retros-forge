package com.gitlab.retropronghorn.retrosforge.controllers;

import java.util.Random;

import com.gitlab.retropronghorn.retrosforge.RetrosForge;
import com.gitlab.retropronghorn.retrosforge.items.DepletedNetherStar;
import com.gitlab.retropronghorn.retrosforge.particle.ParticleEffect;
import com.gitlab.retropronghorn.retrosutils.sound.USound;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Furnace;
import org.bukkit.inventory.FurnaceInventory;
import org.bukkit.inventory.ItemStack;

public class FurnaceController {

    /**
     * Remove a nether star from a furnace and do any effects / sounds
     * @param furnace furnace to deplete from
     */
    public static void depleteNetherStar(Furnace furnace) {
        Location location = furnace.getLocation();
        World world = location.getWorld();
        FurnaceInventory furnaceInventory = furnace.getInventory();
        ItemStack contents = furnaceInventory.getSmelting();
        // Remove a single nether star
        contents.setAmount(contents.getAmount() - 1);
        furnaceInventory.setSmelting(contents);
        // Add depleted nether star to output
        if (furnaceInventory.getResult() == null) {
            furnaceInventory.setResult(DepletedNetherStar.get());
        } else {
            world.dropItem(location, DepletedNetherStar.get());
        }
        // Play a fun little sound
        world.playSound(
            location,
            USound.soundOrFallback(
                RetrosForge.getPlugin().getConfig().getString("sounds.nether-star-depleted")
            ),
            1F,
            1F
        );
    }

    /**
     * Multiplies the yield of a furnace by a random amount in a range
     * @param furnace furnace to multiply yield on
     * @param smelting item to multiply in yield
     */
    public static void multiplyYield(Furnace furnace, ItemStack smelting) {
        Random rand = new Random();
        int yield = rand.nextInt(RetrosForge.getPlugin().getConfig().getInt("max-yield"));
        FurnaceInventory furnaceInventory = furnace.getInventory();
        ItemStack result = furnaceInventory.getResult();
        Location location = furnace.getLocation();
        World world = furnace.getWorld();
        if (result != null)
            result.setAmount(result.getAmount()+yield);
        // Play a fun little sound
        world.playSound(
            location,
            USound.soundOrFallback(
                RetrosForge.getPlugin().getConfig().getString("sounds.smelt")
            ),
            1F,
            1F
        );
        ParticleEffect.createHelix(location);
    }
}