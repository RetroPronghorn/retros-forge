package com.gitlab.retropronghorn.retrosforge.controllers;

import com.gitlab.retropronghorn.retrosforge.RetrosForge;
import com.gitlab.retropronghorn.retrosutils.sound.USound;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

public class PickupController {
    public static void invalidPickup(Location location, Block block) {
        location.getWorld().strikeLightning(location);
        block.setType(Material.AIR);
        block.getWorld().createExplosion(location, 16F);
        block.getWorld().strikeLightning(location);
        location.getWorld().playSound(
            location,
            USound.soundOrFallback(
                RetrosForge.getPlugin().getConfig().getString("sounds.detonate")
            ),
            1F,
            1F
        );
    }

    public static void pickup(Location location) {
        ItemStack netherStar = new ItemStack(Material.NETHER_STAR);
        location.getWorld().dropItem(location, netherStar);
    }
}